<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()->count(rand(1, 3))->create()->each(
            function ($category) {
                Category::factory()->count(rand(1, 3))->create(['parent_id' => $category->id]);
            }
        );
    }
}
