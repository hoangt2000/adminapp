<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Mr.A',
                'email' => 'a@deha-soft.com',
                'password' => bcrypt(12345678),
            ],
            [
                'name' => 'Mr.B',
                'email' => 'b@deha-soft.com',
                'password' => bcrypt(12345678),
            ],
            [
                'name' => 'Mr.C',
                'email' => 'c@deha-soft.com',
                'password' => bcrypt(12345678),
            ],
        ]);
    }
}
