<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'index-role',
                'display_name' => 'Index Role',
            ],
            [
                'name' => 'create-role',
                'display_name' => 'Create Role',
            ],
            [
                'name' => 'edit-role',
                'display_name' => 'Edit Role',
            ],
            [
                'name' => 'delete-role',
                'display_name' => 'Delete Role',
            ],
            [
                'name' => 'index-user',
                'display_name' => 'Index User',
            ],
            [
                'name' => 'create-user',
                'display_name' => 'Create User',
            ],
            [
                'name' => 'edit-user',
                'display_name' => 'Edit User',
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Delete User',
            ],
            [
                'name' => 'index-category',
                'display_name' => 'Index Category',
            ],
            [
                'name' => 'create-category',
                'display_name' => 'Create Category',
            ],
            [
                'name' => 'edit-category',
                'display_name' => 'Edit Category',
            ],
            [
                'name' => 'delete-category',
                'display_name' => 'Delete Category',
            ],
            [
                'name' => 'index-product',
                'display_name' => 'Index Product',
            ],
            [
                'name' => 'create-product',
                'display_name' => 'Create Product',
            ],
            [
                'name' => 'edit-product',
                'display_name' => 'Edit Product',
            ],
            [
                'name' => 'delete-product',
                'display_name' => 'Delete Product',
            ],
        ]);
    }
}
