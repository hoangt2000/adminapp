@extends('layouts.app')
@section('content')
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('users.index')}}">User</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h2 class="mb-0 pl-5">Edit User</h2>
                        <form action="{{ route('users.update', $user->id) }}" method="post"
                              class="pl-5 pr-5 pt-3">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="name">Name :</label>
                                <input type="text" class="form-control" name="name" placeholder="Name"
                                       value="{{$user->name}}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{$errors->first('name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="display_name">Email:</label>
                                <input type="email" class="form-control" name="email" placeholder="Email"
                                       value="{{$user->email}}">
                                @if($errors->has('email'))
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div>
                                @foreach($roles as $role)
                                    <div class="form-check">
                                        <input class="form-check-input"
                                               type="checkbox" value="{{ $role->id }}"
                                               @if(in_array($role->id,$user->roles()->pluck('id')->toArray())) checked
                                               @endif
                                               name="role_ids[]"
                                        >
                                        <label class="form-check-label">
                                            {{ $role->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="{{ route('users.index') }}" class="btn btn-primary float-right"><i
                                    class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

