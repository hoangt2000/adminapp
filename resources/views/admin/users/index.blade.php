@extends('layouts.app')
@section('content')
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">User</a></li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    @hasPermission('create-user')
                    <div class="col-lg-6 col-5 text-right">
                        <a href="{{ route('users.create') }}" class="btn btn-default">Create New User</a>
                    </div>
                    @endhasPermission
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">User</h3>
                        <form action="{{ route('users.index') }}" class="p-3 py-4 mt-3" method="get">
                            <div class="row g-3 mt-2">
                                <div class="col-md-3">
                                    <select class="form-select form-control" aria-label="Default select example"
                                            name="role_id_search">
                                        <option selected value=" ">Select Role</option>
                                        @foreach($roles as $role)
                                            <option {{ ($role->id == request('role_id_search')) ? 'selected' : '' }}
                                                    value="{{ $role->id }}">
                                                {{ $role->display_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6"><input type="search" class="form-control"
                                                             placeholder="Enter Gmail ..." name="email_search"
                                                             value="{{request('email_search')}}"></div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn bg-gradient-warning btn-block">Search Results</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('layouts.alert.alert1')
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            @forelse($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td class="name text-sm"><strong>{{ $user->name }}</strong></td>
                                    <td class="name text-sm"><strong>{{ $user->email }}</strong></td>
                                    <td>
                                        @foreach($user->roles as $role)
                                            <span class="badge badge-success">{{$role->name}}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-left">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                @hasPermission('edit-user')
                                                <a class="dropdown-item"
                                                   href="{{route('users.edit', $user->id)}}">Edit</a>
                                                @endhasPermission
                                                @hasPermission('delete-user')
                                                <a class="dropdown-item deleteButton"
                                                   data-action="{{$user->id}}">Delete</a>
                                                @endhasPermission
                                                <form id="delete-{{ $user->id }}"
                                                      action="{{ route('users.destroy',$user->id) }}"
                                                      method="post" style="display: none">
                                                    @csrf
                                                    @method('delete')
                                                    <input type="hidden" value="{{ $user->id }}" name="user_id">
                                                </form>
                                                <a class="dropdown-item" href="{{route('users.show', $user->id)}}">Detail</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <p class="font-weight-bold"> -- NOT FOUND DATA -- </p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- Card footer -->
                    <div class="card-footer py-4">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="module" src="{{ asset('admin/js/user.js') }}"></script>
@endpush
