@extends('layouts.app')
@section('content')
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('users.index')}}">User</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Create New User</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h2 class="mb-0 pl-5">Create New User</h2>
                        <form action="{{ route('users.store') }}" method="post" class="pl-5 pr-5 pt-3">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name :</label>
                                <input type="text" class="form-control" name="name" placeholder="Name"
                                       value="{{old('name')}}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{$errors->first('name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="email">Email :</label>
                                <input type="email" class="form-control" name="email" placeholder="Email"
                                       value="{{old('email')}}">
                                @if($errors->has('email'))
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password">Password :</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                @if($errors->has('password'))
                                    <p class="text-danger">{{$errors->first('password')}}</p>
                                @endif
                            </div>
                            <div>
                                <p><strong>Role:</strong></p>
                                @foreach($roles as $role)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="{{ $role->id }}"
                                               name="role_ids[]">
                                        <label class="form-check-label">
                                            {{ $role->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success mt-3">Create</button>
                            <a href="{{ route('users.index') }}" class="btn btn-primary float-right mt-3"><i
                                    class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
