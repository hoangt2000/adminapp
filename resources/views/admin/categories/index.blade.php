@extends('layouts.app')
@section('content')
    <!-- Modal Create -->
    <div class="modal fade" id="CreateCategoryModal"
         tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="createCategoryForm">
                        <div class="form-group mb-3">
                            <label for="">Name :</label>
                            <input type="text" class="form-control" id="nameCreate" name="name">
                            <span class="text-danger" id="create_name_error"></span>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Parent Category :</label>
                            <select class="custom-select parent-categories" id="parentIdCreate"
                                    data-action="{{route('categories.getParentCategories')}}" name="parent_id">
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeForm"
                            data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary createCategory"
                            data-action="{{ route('categories.store') }}">Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Edit -->
    <div class="modal fade" id="EditCategoryModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="updateCategoryForm">
                        <div class="form-group mb-3">
                            <label for="">Name :</label>
                            <input type="text" class="name form-control" id="nameUpdate" name="name">
                            <span class="text-danger" id="update_name_error"></span>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Parent Category :</label>
                            <select class="custom-select parent-categories" name="parent_id" id="parentIdUpdate">
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeForm"
                            data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary updateCategory"
                            id="updateCategorySubmit">Update</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">Category</a></li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    @hasPermission('create-category')
                    <div class="col-lg-6 col-5 text-right">
                        <a href="" class="btn btn-default" data-toggle="modal"
                           data-target="#CreateCategoryModal">Create New Category</a>
                    </div>
                    @endhasPermission
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div id="list" data-action="{{route('categories.list')}}">
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script  type="module" src="{{ asset('admin/js/category.js') }}"></script>
@endpush
