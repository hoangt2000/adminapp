<div id="list" data-action="{{route('categories.list')}}">
    <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
            <h3 class="mb-0">Category</h3>

        </div>
        <!-- Light table -->
        <div class="table-responsive">
            <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Main Category</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody class="list">
                @forelse($categories as $category)
                    <tr>
                        <th scope="row">{{ $category->id }}</th>
                        <td class="name text-sm"><strong>{{ $category->name }}</strong></td>
                        <td>
                            {{ $category->parent_category_name }}
                        </td>
                        <td class="text-left">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    @hasPermission('edit-category')
                                    <a class="dropdown-item editCategory" data-action="{{route('categories.edit', $category->id)}}">Edit</a>
                                    @endhasPermission
                                    @hasPermission('delete-category')
                                    <a class="dropdown-item deleteCategory" data-action="{{route('categories.destroy', $category->id)}}">Delete</a>
                                    @endhasPermission
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <p class="font-weight-bold"> -- NOT FOUND DATA -- </p>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Card footer -->
        <div class="card-footer py-4">
            {{ $categories->links() }}
        </div>
    </div>
</div>
