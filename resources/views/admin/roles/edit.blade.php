@extends('layouts.app')
@section('content')
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Role</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Role</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h2 class="mb-0 pl-5">Edit Role {{ $role->display_name }}</h2>
                        <form action="{{ route('roles.update', $role->id) }}" method="post" class="pl-5 pr-5 pt-3">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="name">Name :</label>
                                <input type="text" class="form-control" name="name" placeholder="Name"
                                       value="{{$role->name}}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{$errors->first('name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="display_name">Display Name :</label>
                                <input type="text" class="form-control" name="display_name"
                                       placeholder="Display Name" value="{{$role->display_name}}">
                                @if($errors->has('display_name'))
                                    <p class="text-danger">{{$errors->first('display_name')}}</p>
                                @endif
                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-sm-2">Permission:</div>
                                </div>
                                <br>
                                <h4>Role</h4>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="role-select-all">
                                    <label class="form-check-label">
                                        Select all
                                    </label>
                                </div>
                                @foreach($rolePermissions as $rolePermission)
                                    <div class="form-check form-check-inline rolePerCheck">
                                        <input class="form-check-input rolePermission" type="checkbox"
                                               value="{{ $rolePermission->id }}" name="permission_ids[]"
                                               @if(in_array($rolePermission->id,$role->permissions()->pluck('id')->toArray())) checked
                                               @endif>
                                        <label class="form-check-label">
                                            {{ $rolePermission->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                                <h4>User</h4>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="user-select-all">
                                    <label class="form-check-label">
                                        Select all
                                    </label>
                                </div>
                                @foreach($userPermissions as $userPermission)
                                    <div class="form-check form-check-inline userPerCheck">
                                        <input class="form-check-input userPermission" type="checkbox"
                                               value="{{ $userPermission->id }}" name="permission_ids[]"
                                               @if(in_array($userPermission->id,$role->permissions()->pluck('id')->toArray())) checked
                                               @endif
                                        >
                                        <label class="form-check-label">
                                            {{ $userPermission->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                                <h4>Category</h4>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="category-select-all">
                                    <label class="form-check-label">
                                        Select all
                                    </label>
                                </div>
                                @foreach($categoryPermissions as $categoryPermission)
                                    <div class="form-check form-check-inline categoryPerCheck">
                                        <input class="form-check-input categoryPermission" type="checkbox"
                                               value="{{ $categoryPermission->id }}" name="permission_ids[]"
                                               @if(in_array($categoryPermission->id,$role->permissions()->pluck('id')->toArray())) checked
                                               @endif
                                        >
                                        <label class="form-check-label">
                                            {{ $categoryPermission->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                                <h4>Product</h4>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="product-select-all">
                                    <label class="form-check-label">
                                        Select all
                                    </label>
                                </div>
                                @foreach($productPermissions as $productPermission)
                                    <div class="form-check form-check-inline productPerCheck">
                                        <input class="form-check-input productPermission" type="checkbox"
                                               value="{{ $productPermission->id }}" name="permission_ids[]"
                                               @if(in_array($productPermission->id,$role->permissions()->pluck('id')->toArray())) checked
                                               @endif
                                        >
                                        <label class="form-check-label">
                                            {{ $productPermission->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="{{ route('roles.index') }}" class="btn btn-primary float-right"><i
                                    class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="module" src="{{ asset('admin/js/role.js') }}"></script>
@endpush

