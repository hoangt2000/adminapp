@extends('layouts.app')
@section('content')
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('products.index')}}">Product</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Product</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h2 class="mb-0 pl-5">Create New Product</h2>
                        <form action="{{route('products.update',$product->id)}}" method="post"
                              enctype="multipart/form-data" class="pl-5 pr-5 pt-3">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name :</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{$product->name}}">
                                        @if($errors->has('name'))
                                            <p class="text-danger">{{$errors->first('name')}}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price"> Price :</label>
                                        <input type="number" class="form-control" name="price" placeholder="price"
                                               value="{{$product->price}}">
                                        @if($errors->has('price'))
                                            <p class="text-danger">{{$errors->first('price')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="select2 form-control " name="category_ids[]" multiple="multiple">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"
                                                @foreach($product->categories as $p) @if($category->id == $p->id)selected="selected"@endif @endforeach>{{$category->name}}</option>
                                    @endforeach
                                    @if($errors->has('category_ids'))
                                        <p class="text-danger">{{$errors->first('category_ids')}}</p>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image"> Image :</label>
                                <input type="file" class="form-control" name="image" placeholder="image" id="image">
                                @if ($errors->has('image'))
                                    <strong>{{ $errors->first('image') }}</strong>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <a class="btn btn-outline-danger btn-sm close-button">
                                            <span>&times;</span>
                                        </a>
                                        <img class="w-100" id="showImage"
                                             src="{{ !empty($product->image) ? url('images/'.$product->image) : "" }}">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="description">Description :</label>
                                <textarea type="text" class="form-control" name="description">
                                    {{$product->description}}
                                    @if($errors->has('description'))
                                        <p class="text-danger">{{$errors->first('description')}}</p>
                                    @endif
                                </textarea>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success mt-3">Update</button>
                            <a href="{{ route('products.index') }}" class="btn btn-primary float-right mt-3">
                                <i class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="module" src="{{ asset('admin/js/product.js') }}"></script>
@endpush
