@extends('layouts.app')
@section('content')
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">Product</a></li>
                                <li class="breadcrumb-item active" aria-current="page">List</li>
                            </ol>
                        </nav>
                    </div>
                    @hasPermission('create-user')
                    <div class="col-lg-6 col-5 text-right">
                        <a href="{{ route('products.create') }}" class="btn btn-default">Create New Product</a>
                    </div>
                    @endhasPermission
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Product</h3>
                        <form action="{{ route('products.index') }}" class="py-4 mt-3" method="get">
                            <div class="row g-3 mt-2">
                                <div class="col-md-2">
                                    <select class="form-select form-control" aria-label="Default select example"
                                            name="category_id_search">
                                        <option selected value=" ">Categories</option>
                                        @foreach($categories as $category)
                                            <option
                                                {{ ($category->id == request('category_id_search')) ? 'selected' : '' }}
                                                value="{{ $category->id }}">
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="search" class="form-control" placeholder="Enter Name ..."
                                           name="name_search" value="{{request('name_search')}}">
                                </div>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="min_price_search"
                                           placeholder="Min..." value="{{request('min_price_search')}}">
                                    <br>
                                    <input type="number" class="form-control" name="max_price_search"
                                           placeholder="Max..." value="max_price_search">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn bg-gradient-warning btn-block">
                                        Search Results</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Light table -->
                    <div>
                        @include('layouts.alert.alert1')
                        <table class="mytable align-items-center table-flush table-hover">
                            <thead class="thead">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Image</th>
                                <th scope="col">Price</th>
                                <th scope="col">Category</th>
                                <th scope="col" style="width: 30%;">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            @forelse($products as $product)
                                <tr>
                                    <th scope="row">{{ $product->id }}</th>
                                    <td class="name text-sm">{{ $product->name }}</td>
                                    <td><img class="avatar avatar-lg me-3"
                                             src="{{ $product->image == 'default.png' ? url('images/default.png') : url('images/'.$product->image)}}">
                                    </td>
                                    <td class="name text-sm">{{ $product->price }}</td>
                                    <td>
                                        @foreach($product->categories as $category)
                                            <span class="badge badge-primary">{{$category->name}}</span>
                                        @endforeach
                                    </td>
                                    <td><div class="desover">
                                            {!! substr(strip_tags($product->description), 0, 200) !!}
                                        </div>
                                    </td>
                                    <td class="text-left">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#"
                                               role="button" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                @hasPermission('edit-product')
                                                <a class="dropdown-item"
                                                   href="{{route('products.edit', $product->id)}}">Edit</a>
                                                @endhasPermission
                                                @hasPermission('delete-product')
                                                <a class="dropdown-item deleteButton" data-action="{{$product->id}}">Delete</a>
                                                <form id="delete-{{ $product->id }}"
                                                      action="{{ route('products.destroy',$product->id) }}"
                                                      method="post" style="display: none">
                                                    @csrf
                                                    @method('delete')
                                                </form>
                                                @endhasPermission
                                                <a class="dropdown-item"
                                                   href="{{route('products.show', $product->id)}}">Detail</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <p class="font-weight-bold"> -- NOT FOUND DATA -- </p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- Card footer -->
                    <div class="card-footer py-4">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="module" src="{{ asset('admin/js/product.js') }}"></script>
@endpush
