import {Delete} from "./delete.js";

$(document).ready(function () {
    $(document).on('click', '.deleteButton', function () {
        Delete.confirm($(this).data('action'));
    })
    $('.select2').select2();
});
