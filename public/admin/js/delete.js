export const Delete = (function () {
    let modules = {};
    modules.alert =function (){
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
        });
    }
    modules.confirm = function (id){
        modules.alert().then((result) => {
            if (result.isConfirmed) {
                $(`#delete-${id}`).submit();
            }
        })
    }
    return modules;
}(window.jQuery, window, document));
