export const Modal = (function (){
    let modules = {};
    modules.close = function (modalName){
        $(`#${modalName}Modal`).modal('hide');
    }
    modules.reset = function (modalName){
        $(`#${modalName}Modal form`).trigger('reset');
        $(`#${modalName}Modal form span`).text('');
    }
    modules.show = function (modalName){
        $(`#${modalName}Modal`).modal('show');
    }
    return modules;
}(window.jQuery, window, document));
