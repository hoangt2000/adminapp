import {Delete} from "./delete.js";
const Image = (function () {
    let modules = {};
    modules.show = function (e){
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#showImage').attr('src', e.target.result);
            $('.close-button').show();
        }
        reader.readAsDataURL(e.target.files['0']);
    }
    modules.close = function (){
        $('#image').val('');
        $('#showImage').attr('src', '')
        $('.close-button').hide();
    }
    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    tinymce.init({
        selector: 'textarea',
        height: 500,
    });
    $(document).ready(function () {
        $('.select2').select2();
    });
    $(document).on('click', '.deleteButton', function (e) {
        e.preventDefault();
        Delete.confirm($(this).data('action'));
    })
    $('#image').change(function (e) {
        Image.show(e);
    })
    $('.close-button').on('click', function () {
        Image.close();
    })
});
