import {BaseAjax} from './base-ajax.js';
import {Delete} from "./delete.js";
import {Modal} from "./modal.js";
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';
export const Category = (function () {
    let modules = {};
    modules.renderParentCategory = function (parentCategories){
        $('.parent-categories').html('<option value=" " selected>Open this select menu</option>');
        $.each(parentCategories, function (key, value) {
            $('.parent-categories')
                .append($("<option class='parent-category'></option>")
                    .val(value.id)
                    .text(value.name));
        });
    }
    modules.fillUpdateModal = function (category){
        $('#nameUpdate').val(category.name);
        $('#parentIdUpdate .parent-category').each(function (){
            if(this.value == category.parent_id) $('#parentIdUpdate').val(this.value);
            if(this.value == category.id) {
                $(`#parentIdUpdate option[value=${this.value}]`).attr('disabled', true);
            };
        });
        $('#updateCategorySubmit').attr("data-action",`categories/${category.id}`);
    }

    function afterCallCategoryApiSuccess() {
        modules.getList();
        modules.getParentCategories();
    }

    modules.getParentCategories = function (){
        let url = $('#parentIdCreate').data('action');
        BaseAjax.callApi(url)
            .done(function (res) {
                modules.renderParentCategory(res.data);
            });
    }

    modules.getList = function (page) {
        let url = (page==undefined)? $('#list').data('action'):page.attr('href');
        BaseAjax.callApi(url)
            .then(function (res) {
                $('#list').replaceWith(res);
            })
    }
    modules.renderValidateError = function (form, errors){
        for(let error in errors)
        {
            $(`#${form}_${error}_error`).text(errors[error][0]);
        }
    }
    modules.create = function (){
        let url = $('.createCategory').data('action');
        let data = $('#createCategoryForm').serializeArray();
        BaseAjax.callApi(url, data, POST)
            .done(function () {
                afterCallCategoryApiSuccess();
                Modal.close('CreateCategory');
            })
            .fail(function (res){
                modules.renderValidateError('create',res.responseJSON.errors);
            });
    }
    modules.update = function (){
        let url = $('#updateCategorySubmit').data('action');
        let data = $('#updateCategoryForm').serializeArray();
        BaseAjax.callApi(url, data, PUT)
            .done(function (res) {
                afterCallCategoryApiSuccess();
                Modal.close('EditCategory');
            })
            .fail(function (res){
                modules.renderValidateError('update',res.responseJSON.errors);
            })
    }

    modules.edit = function (category){
        let url = category.data('action');

        BaseAjax.callApi(url)
            .then(function (res) {
                modules.fillUpdateModal(res.data.category);
            })
        Modal.show('EditCategory');
    }
    modules.confirmDelete = function (category){
        Delete.alert().then((result) => {
            if (result.isConfirmed) {
                modules.delete(category);
            }
        })
    }
    modules.delete = function (category){
        let url = category.data('action');
        let data = {};
        BaseAjax.callApi(url, data, DELETE)
            .then(function () {
                afterCallCategoryApiSuccess();
            })
    }
    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    Category.getList();
    Category.getParentCategories();

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        Category.getList($(this));
    })
    $(document).on('click', '.editCategory', function (e) {
        Category.edit($(this));
    })
    $(document).on('click', '.deleteCategory', function (e) {
        Category.confirmDelete($(this));
    })
    $('.createCategory').on('click', function () {
        Category.create();
    })

    $('.updateCategory').on('click', function () {
        Category.update();
    })
    $('#CreateCategoryModal').on('hide.bs.modal', function (e) {
        Modal.reset('CreateCategory');
    })
    $('#EditCategoryModal').on('hide.bs.modal', function (e) {
        Modal.reset('UpdateCategory');
    })
});
