export const BaseAjax = (function () {
    let modules = {};
    modules.callApi = function (url, data = {}, method = "GET") {
        return $.ajax({
            type: method,
            url: url,
            data: data,
        })
    }
    return modules;
}(window.jQuery, window, document));
$(document).ready(function (){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})
