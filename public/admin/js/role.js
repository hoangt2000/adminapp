import {Delete} from "./delete.js";
const PERMISSION_TYPE = ['role', 'user', 'category', 'product'];
const CheckBox = (function () {
    let modules = {};
    modules.checkAll = function (name, $element) {
        $(`.${name}Permission`).prop("checked", $element.prop("checked"))
    }
    modules.deCheckAll = function (name){
        $(`#${name}-select-all`).prop("checked", false)
        $('.permission-select-all').prop("checked", false)
    }
    modules.checkAllSelected = function (name, $element) {
        if ($element.prop("checked")==false){
            modules.deCheckAll(name);
        }
        if ($(`.${name}Permission:checked`).length == $('.'+name+'PerCheck').length){
            $(`#${name}-select-all`).prop("checked", true)
            modules.checkAllPermissionSelected()
        }
    }
    modules.checkAllPermissionSelected = function (){
        if ($('.selectAllPermission:checked').length == PERMISSION_TYPE.length){
            $('.permission-select-all').prop("checked", true);
        }
    }
    modules.checkAllPermission = function ($element){
        PERMISSION_TYPE.forEach((type) =>{
            $(`#${type}-select-all`).prop("checked", $element.prop("checked"));
            modules.checkAll(type,$(`#${type}-select-all`))
        });
    }
    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    PERMISSION_TYPE.forEach((type) =>{
        CheckBox.checkAllSelected(type, $(`#${type}-select-all`));
    });
    $('.permission-select-all').change(function (){
        CheckBox.checkAllPermission($(this));
    })
    $('#role-select-all').change(function () {
        CheckBox.checkAll('role', $(this));
        CheckBox.checkAllSelected('role', $(this));
    })
    $(".rolePermission").change(function () {
        CheckBox.checkAllSelected('role', $(this));
    })
    $('#user-select-all').change(function () {
        CheckBox.checkAll('user', $(this));
        CheckBox.checkAllSelected('role', $(this));
    })
    $(".userPermission").change(function () {
        CheckBox.checkAllSelected('user', $(this))
    })
    $('#category-select-all').change(function () {
        CheckBox.checkAll('category', $(this));
        CheckBox.checkAllSelected('category', $(this));
    })
    $(".categoryPermission").change(function () {
        CheckBox.checkAllSelected('category', $(this))
    })
    $('#product-select-all').change(function () {
        CheckBox.checkAll('product', $(this));
        CheckBox.checkAllSelected('product', $(this));
    })
    $(".productPermission").change(function () {
        CheckBox.checkAllSelected('product', $(this))
    })
    $(document).on('click', '.deleteButton', function () {
        Delete.confirm($(this).data('action'));
    })
});
