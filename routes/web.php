<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('roles')->name('roles.')->middleware('auth')->group(function(){
    Route::get('/', [RoleController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-role');
    Route::get('/{role}', [RoleController::class, 'show'])
        ->name('show')
        ->where('role', '[0-9]+')
        ->middleware('permission:index-role');
    Route::get('/search', [RoleController::class, 'search'])
        ->name('search')
        ->middleware('permission:index-role');
    Route::get('/create', [RoleController::class, 'create'])
        ->name('create')
        ->middleware('permission:create-role');
    Route::post('/', [RoleController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-role');
    Route::get('/{role}/edit', [RoleController::class, 'edit'])
        ->name('edit')
        ->middleware(['permission:edit-role']);
    Route::put('/{role}', [RoleController::class, 'update'])
        ->name('update')
        ->middleware(['permission:edit-role']);
    Route::delete('/{role}', [RoleController::class, 'destroy'])
        ->name('destroy')
        ->middleware(['permission:delete-role', 'check.superadmin']);
});

Route::prefix('users')->name('users.')->middleware('auth')->group(function(){
    Route::get('/', [UserController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-user');
    Route::get('/{user}', [UserController::class, 'show'])
        ->name('show')
        ->where('user', '[0-9]+')
        ->middleware('permission:index-user');
    Route::get('/search', [UserController::class, 'search'])
        ->name('search')
        ->middleware('permission:index-user');;
    Route::get('/create', [UserController::class, 'create'])
        ->name('create')
        ->middleware('permission:create-user');
    Route::post('/', [UserController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-user');
    Route::get('/{user}/edit', [UserController::class, 'edit'])
        ->name('edit')
        ->middleware(['permission:edit-user']);
    Route::put('/{user}', [UserController::class, 'update'])
        ->name('update')
        ->middleware(['permission:edit-user']);
    Route::delete('/{user}', [UserController::class, 'destroy'])
        ->name('destroy')
        ->middleware(['permission:delete-user', 'check.superadmin', 'check.user']);
});
Route::prefix('categories')->name('categories.')->middleware('auth')->group(function() {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-category');
    Route::get('/list', [CategoryController::class, 'list'])
        ->name('list')
        ->middleware('permission:index-category');
    Route::get('/getParentCategories', [CategoryController::class, 'getParentCategories'])
        ->name('getParentCategories')
        ->middleware('permission:index-category');
    Route::post('/', [CategoryController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-category');
    Route::get('/{category}/edit', [CategoryController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit-category');
    Route::put('/{category}', [CategoryController::class, 'update'])
        ->name('update')
        ->middleware('permission:edit-category');
    Route::delete('/{category}', [CategoryController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:delete-category');
});
Route::prefix('products')->name('products.')->middleware('auth')->group(function() {
    Route::get('/', [ProductController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-product');
    Route::get('/{product}', [ProductController::class, 'show'])
        ->name('show')
        ->where('product', '[0-9]+')
        ->middleware('permission:index-product');
    Route::get('/search', [ProductController::class, 'search'])
        ->name('search')
        ->middleware('permission:index-product');
    Route::get('/create', [ProductController::class, 'create'])
        ->name('create')
        ->middleware('permission:create-product');
    Route::post('/', [ProductController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-product');
    Route::get('/{product}/edit', [ProductController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit-product');
    Route::put('/{product}', [ProductController::class, 'update'])
        ->name('update')
        ->middleware('permission:edit-product');
    Route::delete('/{product}', [ProductController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:delete-product');
});
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');


