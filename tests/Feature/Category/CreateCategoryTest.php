<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function super_admin_can_create_category()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->make()->toArray();
        $response = $this->post(route('categories.store'), $category);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('categories', ['name' => $category['name']]);
        $response->assertJsonFragment(['message' => 'success']);
    }
    /** @test */
    public function super_admin_can_not_create_category_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory([
            'name' => null,
            'categories_id' => $this->getParentCategoryId(),
        ])->make()->toArray();
        $response = $this->post(route('categories.store'), $category);
        $response->assertSessionHasErrors('name');
    }
    /** @test */
    public function authenticated_user_can_not_create_category()
    {
        $this->loginWithUser();
        $category = Category::factory()->make()->toArray();
        $response = $this->post(route('categories.store'), $category);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_have_permission_can_create_category()
    {
        $this->loginUserWithPermission('create-category');
        $category = Category::factory()->make()->toArray();
        $response = $this->post(route('categories.store'), $category);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('categories', ['name' => $category['name']]);
        $response->assertJsonFragment(['message' => 'success']);
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_create_category_if_name_is_null()
    {
        $this->loginUserWithPermission('create-category');
        $category = Category::factory([
            'name' => null,
            'categories_id' => $this->getParentCategoryId(),
        ])->make()->toArray();
        $response = $this->post(route('categories.store'), $category);
        $response->assertSessionHasErrors('name');
    }
    /** @test */
    public function unauthenticated_user_can_not_create_category()
    {
        $category = Category::factory()->make()->toArray();
        $response = $this->post(route('categories.store'), $category);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
