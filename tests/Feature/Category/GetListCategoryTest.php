<?php

namespace Tests\Feature\Category;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function super_admin_can_get_all_categories()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get(route('categories.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.index');
    }
    /** @test */
    public function authenticated_user_can_not_get_all_categories()
    {
        $this->loginWithUser();
        $response = $this->get(route('categories.index'));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_have_permission_can_get_all_categories()
    {
        $this->loginUserWithPermission('index-category');
        $response = $this->get(route('categories.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.index');
    }
    /** @test */
    public function unauthenticated_user_can_not_get_all_categories()
    {
        $response = $this->get(route('categories.index'));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
