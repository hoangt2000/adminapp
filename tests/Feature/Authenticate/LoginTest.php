<?php

namespace Tests\Feature\Authenticate;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function user_can_view_login()
    {
        $response = $this->get('/login');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.login')->assertSee('Login');
    }
    /** @test */
    public function user_can_successfully_log_in()
    {
        $this->assertGuest();
        $user = User::factory()->create(['password' => bcrypt('feature')]);
        $loginData = [
            'email'    => $user->email,
            'password'    => 'feature'
        ];

        $response = $this->post('/login', $loginData);
        $response->assertRedirect('/home');
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertAuthenticatedAs($user);
    }
    /** @test */
    public function user_receives_errors_for_wrong_password()
    {
        $user = User::factory()->create(['password' => bcrypt('laravel')]);

        $loginData = [
            'email'    => $user->email,
            'password'    => 'invalid-password'
        ];

        $response = $this->post('/login', $loginData);
        $response->assertRedirect('/');
        $response->assertSessionHasErrors('email');
        $this->assertGuest();
    }
    /** @test */
    public function user_is_redirected_to_login_page_if_not_logged_in()
    {
        $this->withExceptionHandling();
        $response = $this->get('/home');
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function user_is_redirected_to_dashboard_if_logged_in_and_tries_to_access_login_page()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->get('/login');
        $response->assertRedirect('/home');
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
