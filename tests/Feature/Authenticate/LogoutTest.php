<?php

namespace Tests\Feature\Authenticate;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function user_can_successfully_log_out()
    {
        $response = $this->post('/logout');
        $response->assertRedirect('/');
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertGuest();
    }
}
