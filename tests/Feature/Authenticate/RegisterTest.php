<?php

namespace Tests\Feature\Authenticate;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function user_can_view_register()
    {
        $response = $this->get('/register');

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.register');
        $response->assertSee('register');
    }
    /** @test */
    public function user_can_successfully_register()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $registerData = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ];
        $response = $this->post('/register', $registerData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('users', ['email' => $registerData['email']]);
        $this->assertAuthenticated();
    }
    /** @test */
    public function user_can_not_register_if_name_is_null()
    {
        $user = User::factory()->make();
        $registerData = [
            'name' => '',
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ];
        $response = $this->post('/register', $registerData);
        $response->assertSessionHasErrors('name');
        $this->assertGuest();
    }
    /** @test */
    public function user_can_not_register_if_email_is_null()
    {
        $user = User::factory()->make();
        $registerData = [
            'name' => $user->name,
            'email' => '',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ];
        $response = $this->post('/register', $registerData);
        $response->assertSessionHasErrors('email');
        $this->assertGuest();
    }
    /** @test */
    public function user_can_not_register_if_password_is_null()
    {
        $user = User::factory()->make();
        $registerData = [
            'name' => $user->name,
            'email' => '',
            'password' => '',
            'password_confirmation' => '12345678'
        ];
        $response = $this->post('/register', $registerData);
        $response->assertSessionHasErrors('password');
        $this->assertGuest();
    }
    /** @test */
    public function user_can_not_register_if_confirm_password_is_incorrect()
    {
        $user = User::factory()->make();
        $registerData = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '123456789'
        ];
        $response = $this->post('/register', $registerData);
        $response->assertSessionHasErrors('password');
        $this->assertGuest();
    }

}
