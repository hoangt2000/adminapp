<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetUserTest extends TestCase
{
    /** @test */
    public function super_admin_get_single_user()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get(route('users.show', $user));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.show');
        $response->assertSee($user->name)->assertSee($user->email);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_get_single_user()
    {
        $this->loginWithUser();
        $user = User::factory()->create();
        $response = $this->get(route('roles.show', $user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertDontSee($user->name);
    }
    /** @test */
    public function authenticated_user_have_permission_can_get_single_user()
    {
        $this->loginUserWithPermission('index-role');
        $user = User::factory()->create();
        $response = $this->get(route('users.show', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.show');
        $response->assertSee($user->name);
    }
    /** @test */
    public function unauthenticated_user_can_not_get_single_user()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.show', $user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
