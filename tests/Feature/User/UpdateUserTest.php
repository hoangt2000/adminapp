<?php

namespace Tests\Feature\User;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function super_admin_can_see_edit_form()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.edit');
        $response->assertSee($user->name)->assertSee($user->email);
    }
    /** @test */
    public function super_admin_can_update_user()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $name = Str::random(10);
        $email = Str::random(10) . '@deha-soft.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
        $this->assertDatabaseHas('users', ['name' => $name, 'email' => $email]);
    }
    /** @test */
    public function super_admin_can_not_update_user_name_and_email_are_null()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'email' => null,
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertSessionHasErrors(['name', 'email']);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_edit_user_form()
    {
        $this->loginWithUser();
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_update_user()
    {
        $this->loginWithUser();
        $user = User::factory()->create();
        $name = Str::random(10);
        $email = Str::random(10) . '@gmail.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_have_permission_can_see_edit_user_form()
    {
        $this->loginUserWithPermission('edit-user');
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.edit');
        $response->assertSee('name')->assertSee('email');
    }
    /** @test */
    public function authenticated_user_have_permission_can_update_user()
    {
        $this->loginUserWithPermission('edit-user');
        $user = User::factory()->create();
        $name = Str::random(10);
        $email = Str::random(10) . '@deha-soft.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
        $this->assertDatabaseHas('users', ['name' => $name, 'email' => $email]);
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_update_email_if_name_are_null()
    {
        $this->loginUserWithPermission('edit-user');
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'email' => null
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertSessionHasErrors(['name', 'email']);
        $this->assertDatabaseMissing('users', $data);
    }
    /** @test */
    public function unauthenticated_user_can_not_see_edit_user_form()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function unauthenticated_user_can_not_update_user()
    {
        $user = User::factory()->create();
        $name = Str::random(10);
        $email = Str::random(10) . '@deha-soft.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function user_sync_roles()
    {
        $user = User::find(3);
        $role = Role::find(3);
        $user->roles()->sync($role->pluck('id'));
        $this->assertDatabaseHas('user_role', ['user_id' => $user->id, 'role_id' => $role->id]);
    }
}
