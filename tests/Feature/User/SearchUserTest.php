<?php

namespace Tests\Feature\User;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchUserTest extends TestCase
{
    /** @test */
    public function super_admin_can_search_users_follow_email_and_role()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $role = Role::factory()->create()->pluck('id');
        $role_search = $user->roles()->attach($role);
        $response = $this->get('/users?role_id=' . $role_search . '&email=' . $user->email . '');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.index');
        $response->assertSee($user->email);
    }
    /** @test */
    public function authenticated_user_have_permission_can_search_role_and_email()
    {
        $this->loginUserWithPermission('index-user');
        $user = User::factory()->create();
        $role = Role::factory()->create()->pluck('id');
        $role_search = $user->roles()->attach($role);
        $response = $this->get('/users?role_id=' . $role_search . '&email=' . $user->email . '');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.index');
        $response->assertSee($user->email);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_search_role_and_email()
    {
        $this->loginWithUser();
        $user = User::factory()->create();
        $role = Role::factory()->create()->pluck('id');
        $role_search = $user->roles()->attach($role);
        $response = $this->get('/users?role_id=' . $role_search . '&email=' . $user->email . '');
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function unauthenticated_user_can_not_search_role_and_email()
    {
        $role = Role::factory()->create();
        $user = User::factory()->create();
        $role = Role::factory()->create()->pluck('id');
        $role_search = $user->roles()->attach($role);
        $response = $this->get('/users?role_id=' . $role_search . '&email=' . $user->email . '');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
