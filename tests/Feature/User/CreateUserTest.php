<?php

namespace Tests\Feature\User;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function super_admin_can_see_create_user_form()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get(route('users.create'));
        $response->assertViewIs('admin.users.create');
        $response->assertSee('name')->assertSee('email');
        $response->assertStatus(Response::HTTP_OK);
    }
    /** @test */
    public function super_admin_can_create_new_user()
    {
        $this->loginWithSuperAdmin();
        $name = Str::random(10);
        $email = Str::random(10) . '@deha-soft.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->post(route('users.store'), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
        $this->assertDatabaseHas('users', ['email' => $email, 'name' => $name]);
    }
    /** @test */
    public function super_admin_can_not_create_new_user_if_name_and_email_and_password_are_null()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory([
            'name' => null,
            'email' => null,
            'password' => null])->make()->toArray();
        $response = $this->post(route('users.store'), $user);
        $response->assertSessionHasErrors('name')
            ->assertSessionHasErrors('email')
            ->assertSessionHasErrors('password');
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_create_user_form()
    {
        $this->loginWithUser();
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_create_user()
    {
        $this->loginWithUser();
        $user = User::factory()->make()->toArray();
        $response = $this->get(route('users.store'), $user);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_have_permission_can_see_create_user_form()
    {
        $this->loginUserWithPermission('create-user');
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.create');
        $response->assertSee('name')->assertSee('email');
    }
    /** @test */
    public function authenticated_user_have_permission_can_create_new_user()
    {
        $this->loginUserWithPermission('create-user');
        $name = Str::random(10);
        $email = Str::random(10) . '@deha-soft.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->post(route('users.store'), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
        $this->assertDatabaseHas('users', ['email' => $email, 'name' => $name]);
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_name_and_email_and_password_are_null()
    {
        $this->loginUserWithPermission('create-user');
        $user = User::factory(
            ['name' => null,
                'email' => null,
                'password' => null])
            ->make()->toArray();
        $response = $this->post(route('users.store'), $user);
        $response->assertSessionHasErrors('name')
            ->assertSessionHasErrors('email')
            ->assertSessionHasErrors('password');
    }
    /** @test */
    public function unauthenticated_user_can_not_see_create_new_user_form()
    {
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function unauthenticated_user_can_not_create_new_user()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.store'), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function user_attach_roles()
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $this->assertDatabaseHas('user_role', ['user_id' => $user->id, 'role_id' => $role->id]);
    }
}
