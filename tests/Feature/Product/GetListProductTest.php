<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_get_all_products()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.index');
        $response->assertSee($product->name)->assertSee($product->price);
    }
    /** @test */
    public function unauthenticated_super_admin_can_not_get_all_products()
    {
        $product = Product::factory()->create();
        $response = $this->get(route('products.index'));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_get_all_products()
    {
        $this->loginWithUser();
        $product = Product::factory()->create();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertDontSee($product->name);
    }
    /** @test */
    public function authenticated_user_have_permission_can_get_all_products()
    {
        $this->loginUserWithPermission('index-product');
        $product = Product::factory()->create();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.index');
        $response->assertSee($product->name);

    }
}
