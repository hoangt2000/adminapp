<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_can_see_create_product_form()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get(route('products.create'));
        $response->assertViewIs('admin.products.create');
        $response->assertSee('name')->assertSee('price');
        $response->assertStatus(Response::HTTP_OK);
    }
    /** @test */
    public function authenticated_super_admin_can_create_new_product()
    {

        $this->loginWithSuperAdmin();
        $data = [
            'name' => Str::random(10),
            'price' => random_int(100, 9999),
            'description' => 'ok',
            'image' => File::image('test.png')
        ];
        $response = $this->post(route('products.store'), $data);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }
    /** @test */
    public function product_be_long_category()
    {
        $product = Product::factory()->create();
        $category = Category::factory()->create();
        $product->categories()->sync($category->pluck('id'));
        $this->assertDatabaseHas('category_product', ['category_id' => $category->id, 'product_id' => $product->id]);
    }
    /** @test */
    public function authenticated_super_admin_can_not_create_new_product_if_name_and_price_is_null()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();
        $response = $this->post(route('products.store'), $product);
        $response->assertSessionHasErrors('name')->assertSessionHasErrors('price');

    }
    /** @test */
    public function unauthenticated_user_can_not_see_create_product_form()
    {
        $response = $this->get(route('products.create'));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_create_new_product()
    {
        $this->loginWithUser();
        $response = $this->get(route('products.create'));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_have_permission_can_see_create_product_form()
    {
        $this->loginUserWithPermission('create-product');
        $response = $this->get(route('products.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.create');
        $response->assertSee('name')->assertSee('price');
    }
    /** @test */
    public function authenticated_user_have_permission_can_create_new_product()
    {
        $this->loginUserWithPermission('create-product');
        $data = [
            'name' => Str::random(10),
            'price' => random_int(100, 9999),
            'description' => 'ok',
            'image' => File::image('test.png')
        ];
        $response = $this->post(route('products.store'), $data);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_product_if_name_and_price_are_null()
    {
        $this->loginUserWithPermission('create-user');
        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();
        $response = $this->post(route('products.store'), $product);
        $response->assertSessionHasErrors('name')->assertSessionHasErrors('price');
    }
}
