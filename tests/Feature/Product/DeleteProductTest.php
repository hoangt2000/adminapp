<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function super_admin_can_delete_product()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->delete(route('products.destroy', $product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }
    /** @test */
    public function authenticated_user_have_permission_can_delete_product()
    {
        $this->loginUserWithPermission('delete-product');
        $product = Product::factory()->create();
        $response = $this->delete(route('products.destroy', $product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }
    /** @test */
    public function unauthenticated_super_admin_can_not_delete_product()
    {
        $product = Product::factory()->create();
        $response = $this->delete(route('products.destroy', $product));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
