<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_can_see_edit_product_form()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.edit');
        $response->assertSee($product->name)->assertSee($product->price);
    }
    /** @test */
    public function authenticated_super_admin_can_update_product()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $data = [
            'name' => Str::random(10),
            'price' => random_int(100, 9999),
            'description' => 'ok',
            'image' => File::image('test.png'),
        ];
        $response = $this->put(route('products.update', $product->id), $data);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }
    /** @test */
    public function authenticated_super_admin_can_not_update_product_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $data = [
            'name' => null,
        ];
        $response = $this->put(route('products.update', $product), $data);
        $response->assertSessionHasErrors('name');
    }
    /** @test */
    public function authenticated_super_admin_can_not_update_product_price_is_null()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $data = [
            'price' => null,
        ];
        $response = $this->put(route('products.update', $product), $data);
        $response->assertSessionHasErrors('price');
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_edit_product_form()
    {
        $this->loginWithUser();
        $product = Product::factory()->create();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_update_product()
    {
        $this->loginWithUser();
        $product = Product::factory()->create();
        $data = [
            'name' => Str::random(10),
            'price' => random_int(100, 9999),
            'description' => 'ok',
            'image' => 'default.png',
        ];
        $response = $this->put(route('products.update', $product->id), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_update_name_if_price_are_null()
    {
        $this->loginUserWithPermission('edit-user');
        $product = Product::factory()->create();
        $data = [
            'name' => null,
            'price' => null
        ];
        $response = $this->put(route('products.update', $product->id), $data);
        $response->assertSessionHasErrors(['name', 'price']);
        $this->assertDatabaseMissing('products', $data);
    }
    /** @test */
    public function authenticated_user_have_permission_can_see_edit_product_form()
    {
        $this->loginUserWithPermission('edit-product');
        $product = Product::factory()->create();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.edit');
        $response->assertSee('name')->assertSee('price');
    }
}
