<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_get_single_product()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get(route('products.show', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.show');
        $response->assertSee($product->name);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_get_single_product()
    {
        $this->loginWithUser();
        $product = Product::factory()->create();
        $response = $this->get(route('products.show', $product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertDontSee($product->name);
    }
    /** @test */
    public function authenticated_user_have_permission_can_get_single_product()
    {
        $this->loginUserWithPermission('index-product');
        $product = Product::factory()->create();
        $response = $this->get(route('products.show', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.show');
        $response->assertSee($product->name);
    }
}
