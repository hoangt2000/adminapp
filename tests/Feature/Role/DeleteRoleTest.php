<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_delete_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.destroy', $role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('roles',['id' => $role->id]);
        $response->assertRedirect(route('roles.index'));
    }
    /** @test */
    public function authenticated_user_have_permission_can_delete_role()
    {
        $this->loginUserWithPermission('delete-role');
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.destroy', $role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_delete_role_super_admin()
    {
        $this->loginUserWithPermission('delete-role');
        $role = Role::where('name', 'super-admin')->first();
        $response = $this->delete(route('roles.destroy', $role->id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $this->assertDatabaseHas('roles', $role->toArray());
    }
    /** @test */
    public function super_admin_can_not_delete_role_super_admin()
    {
        $this->loginWithSuperAdmin();
        $role = Role::where('name', 'super-admin')->first();
        $response = $this->delete(route('roles.destroy', $role->id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $this->assertDatabaseHas('roles', $role->toArray());
    }
    /** @test */
    public function authenticated_user_can_not_delete_role()
    {
        $this->loginWithUser();
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.destroy', $role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('roles', $role->toArray());
    }
    /** @test */
    public function unauthenticated_user_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.destroy', $role->id));
        $this->assertDatabaseHas('roles', $role->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
