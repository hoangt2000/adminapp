<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_get_all_roles()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->display_name);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_get_all_roles()
    {
        $this->loginWithUser();
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertDontSee($role->display_name);
    }
    /** @test */
    public function authenticated_user_have_permission_can_get_all_roles()
    {
        $this->loginUserWithPermission('index-role');
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->display_name);

    }
    /** @test */
    public function unauthenticated_user_can_not_get_all_roles()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }

    /**
     * @return string
     */
    public function getRoleRoute(): string
    {
        return route('roles.index');
    }
}
