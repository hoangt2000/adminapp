<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function super_admin_can_see_edit_role_form()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.edit');
    }
    /** @test */
    public function super_admin_can_update_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $name = Str::random(10);
        $data = [
            'name' => Str::slug($name, '-'),
            'display_name' => $name,
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $data);
    }
    /** @test */
    public function super_admin_can_not_update_role_if_name_and_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'display_name' => null
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertSessionHasErrors(['name', 'display_name']);
        $this->assertDatabaseMissing('roles', $data);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_edit_form()
    {
        $this->loginWithUser();
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
    /** @test */
    public function authenticated_user_not_have_permission_can_not_update_role()
    {
        $this->loginWithUser();
        $role = Role::factory()->create();
        $data = [
            'name' => Str::random(10),
            'display_name' => Str::random(10),
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseMissing('roles', $data);
    }
    /** @test */
    public function authenticated_user_have_permission_can_see_edit_form()
    {
        $this->loginUserWithPermission('edit-role');
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.edit');
    }
    /** @test */
    public function authenticated_user_have_permission_can_update_role()
    {
        $this->loginUserWithPermission('edit-role');
        $role = Role::factory()->create();
        $name = Str::random(10);
        $data = [
            'name' => Str::slug($name, '-'),
            'display_name' => $name,
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $data);
    }
    /** @test */
    public function authenticated_user_have_permission_can_not_update_role_if_name_and_display_name_are_null()
    {
        $this->loginUserWithPermission('edit-role');
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'display_name' => null
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertSessionHasErrors(['name', 'display_name']);
        $this->assertDatabaseMissing('roles', $data);
    }
    /** @test */
    public function unauthenticated_user_can_not_see_edit_role_form()
    {
        $role = Role::factory()->create();
        $response = $this->get(route('roles.edit', $role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function unauthenticated_user_can_not_update_role()
    {
        $role = Role::factory()->create();
        $name = Str::random(10);
        $data = [
            'name' => Str::slug($name, '-'),
            'display_name' => $name,
        ];
        $response = $this->put(route('roles.update', $role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $this->assertDatabaseMissing('roles', $data);
    }
}
