<?php

namespace Tests;

use App\Models\Category;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker;
    protected function getParentCategoryId()
    {
        return Category::withParentCategories()->get()->random(1)->pluck('id');
    }

    protected function loginWithSuperAdmin()
    {
        $user = User::find(1);
        return $this->actingAs($user);
    }
    protected function loginWithUser()
    {
        $user = User::factory()->create();
        return $this->actingAs($user);
    }
    protected function loginUserWithPermission($permission)
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $permission = Permission::where('name', $permission)->first();
        $role->permissions()->attach($permission->pluck('id'));
        return $this->actingAs($user);
    }
}
