<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function count()
    {
        return $this->roleRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name_search'] = $request->name_search ?? '';
        return $this->roleRepository->search($dataSearch)->appends($request->all());
    }

    public function getRoleWithOutSuperAdmin()
    {
        return $this->roleRepository->getRoleWithOutSuperAdmin();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['permission_ids'] = $request->permission_ids ?? [];
        $role = $this->roleRepository->create($dataCreate);
        $role->assignPermissions($dataCreate['permission_ids']);
        return $role;
    }

    public function findById($id)
    {
        return $this->roleRepository->findById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $dataUpdate['permission_ids'] = $request->permission_ids ?? [];
        return  $this->roleRepository->update($dataUpdate,$id)->syncPermissions($dataUpdate['permission_ids']);
    }

    public function delete($id)
    {
        return $this->roleRepository->destroy($id);
    }
}
