<?php

namespace App\Services;

use App\Http\Resources\CategoryCollection;
use App\Repositories\CategoryRepository;

class CategoryService
{

    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function count()
    {
        return $this->categoryRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['parent_id'] = $request->parent_id ?? '';
        return $this->categoryRepository->search($dataSearch);
    }

    public function getParentCategories()
    {
        return $this->categoryRepository->getParentCategories();
    }

    public function getCategories()
    {
        return $this->categoryRepository->all();
    }

    public function create($request)
    {
        return $this->categoryRepository->create($request->all());
    }

    public function findById($id)
    {
        return $this->categoryRepository->findById($id);
    }

    public function update($request, $id)
    {
        return $this->categoryRepository->update($request->all(),$id);
    }

    public function delete($id)
    {
        return $this->categoryRepository->destroy($id);
    }
}
