<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function count()
    {
        return $this->userRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['email_search'] = $request->email_search ?? '';
        $dataSearch['role_id_search'] = $request->role_id_search ?? '';
        return $this->userRepository->search($dataSearch)->appends($request->all());
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($dataCreate['password']);
        $dataCreate['role_ids'] = $request->role_ids ?? [];
        $user = $this->userRepository->create($dataCreate);
        $user->assignRoles($dataCreate['role_ids']);
        return $user;
    }

    public function findById($id)
    {
        return $this->userRepository->findById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $dataUpdate['role_ids'] = $request->role_ids ?? [];

        return $this->userRepository->update($dataUpdate, $id)->syncRoles($dataUpdate['role_ids']);
    }

    public function delete($id)
    {
        return $this->userRepository->destroy($id);
    }
}
