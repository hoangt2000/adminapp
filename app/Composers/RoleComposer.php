<?php

namespace App\Composers;

use App\Services\RoleService;
use Illuminate\View\View;

class RoleComposer
{
    public function __construct(RoleService $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('roles', $this->roles->getRoleWithOutSuperAdmin());
    }
}
