<?php

namespace App\Composers;

use App\Services\CategoryService;
use Illuminate\View\View;

class CategoryComposer
{
    public function __construct(CategoryService $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->categories->getCategories());
    }
}
