<?php

namespace App\Composers;

use App\Services\PermissionService;
use Illuminate\View\View;

class PermissionComposer
{
    public function __construct(PermissionService $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('rolePermissions', $this->permissions->getPermissionsWithName('role'));
        $view->with('userPermissions', $this->permissions->getPermissionsWithName('user'));
        $view->with('categoryPermissions', $this->permissions->getPermissionsWithName('category'));
        $view->with('productPermissions', $this->permissions->getPermissionsWithName('product'));
    }
}
