<?php

namespace App\Providers;

use App\Composers\CategoryComposer;
use App\Composers\PermissionComposer;
use App\Composers\RoleComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /* role view */
        View::composer('admin.roles.create', PermissionComposer::class);
        View::composer('admin.roles.edit', PermissionComposer::class);
        /* user view */
        View::composer('admin.users.index', RoleComposer::class);
        View::composer('admin.users.create', RoleComposer::class);
        View::composer('admin.users.edit', RoleComposer::class);
        /* product view */
        View::composer('admin.products.index', CategoryComposer::class);
        View::composer('admin.products.create', CategoryComposer::class);
        View::composer('admin.products.edit', CategoryComposer::class);
    }
}
