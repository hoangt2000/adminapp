<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CategoryRequest;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function Symfony\Component\Translation\t;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        return view('admin.categories.index');
    }

    public function list(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('admin.categories.list', compact('categories'));
    }

    public function getParentCategories()
    {
        $parentCategories = $this->categoryService->getParentCategories();
        $parentCategoriesCollection = new CategoryCollection($parentCategories);
        return $this->sentSuccessResponse($parentCategoriesCollection,
            'success', Response::HTTP_OK);
    }

    public function store(CreateCategoryRequest $request)
    {
        $category = $this->categoryService->create($request);
        $categoryResource = new CategoryResource($category);
        return $this->sentSuccessResponse($categoryResource, 'success', Response::HTTP_OK);
    }

    public function edit($id)
    {
        $category = $this->categoryService->findById($id);
        $parentCategories = $this->categoryService->getParentCategories();
        $categoryResource = new CategoryResource($category);
        $parentCategoriesResource = new CategoryCollection($parentCategories);
        return $this->sentSuccessResponse([
            'category' => $categoryResource,
            'parentCategories' => $parentCategoriesResource
        ],
            'success',
            Response::HTTP_OK);
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = $this->categoryService->update($request, $id);
        $categoryResource = new CategoryResource($category);
        return $this->sentSuccessResponse($categoryResource, 'success', Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $category = $this->categoryService->delete($id);
        $categoryResource = new CategoryResource($category);
        return $this->sentSuccessResponse($categoryResource, 'success', Response::HTTP_OK);
    }
}
