<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }


    public function store(CreateUserRequest $request)
    {
        $this->userService->create($request);
        return redirect()->route('users.index')
            ->with('message', 'Create User Success !!!');
    }

    public function show($id)
    {
        $user = $this->userService->findById($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->findById($id);
        return view('admin.users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index')
            ->with('message', 'Update User Success !!!');
    }

    public function destroy($id)
    {
        $this->userService->delete($id);
        return redirect()->route('users.index')
            ->with('message', 'Delete User Success !!!');
    }
}
