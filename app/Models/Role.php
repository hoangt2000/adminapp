<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
        'name',
        'display_name',
    ];

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('display_name', 'LIKE', "%{$name}%") : null;
    }

    public function scopeWithNameNotSuperAdmin($query)
    {
        return $query->where('name', '!=', 'super-admin');
    }

    public function assignPermissions($permissionIds)
    {
        return $this->permissions()->attach($permissionIds);
    }

    public function syncPermissions($permissionIds)
    {
        return $this->permissions()->sync($permissionIds);
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'user_role',
            'role_id',
            'user_id'
        );
    }

    public function permissions()
    {
        return $this->belongsToMany(
            Permission::class,
            'role_permission',
            'role_id',
            'permission_id'
        );
    }

    public function hasPermission($permissionName)
    {
        return $this->permissions->contains('name', $permissionName);
    }
}
