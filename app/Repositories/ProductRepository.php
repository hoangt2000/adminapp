<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model
            ->withCategoryIds($dataSearch['category_id_search'])
            ->withName($dataSearch['name_search'])
            ->withMinPrice($dataSearch['min_price_search'])
            ->withMaxPrice($dataSearch['max_price_search'])
            ->latest('id')
            ->paginate(3);
    }
}
