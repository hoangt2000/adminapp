<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;

    abstract public function model();

    public function all()
    {
        return $this->model->all();
    }

    public function __construct()
    {
        $this->model = app($this->model());
    }

    public function count()
    {
        return $this->model->all()->count();
    }

    public function findById($id)
    {
        return $this->model->find($id);
    }

    public function create($dataCreate)
    {
        return $this->model->create($dataCreate);
    }
    public function update($dataUpdate, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($dataUpdate);
        $model->save();

        return $model;
    }
    public function destroy($id)
    {
        $model = $this->findById($id);
        $model->delete();
        return $model;
    }
}
