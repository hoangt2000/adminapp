<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        return $this->model
            ->withEmail($dataSearch['email_search'])
            ->withRoleId($dataSearch['role_id_search'])
            ->latest('id')
            ->paginate(5);
    }
}
